<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.util.*,java.sql.*" %>
<%@page import="Model.DBconnector"%>
<%@page import="controller.Arranged"%>
<%

response.addHeader("Cache-Control","no-cache,no-store,must-revalidate"); 
response.addHeader("Pragma","no-cache");
response.addDateHeader ("Expires", 0);



%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="Style.css">

</head>
<body>
<div class="Back">
<a href ="Fetchcolumn.jsp">
		<img src = "images/back.png" alt = "back button">
	</a>
</div>
<div class="logout">
<a href="#" onclick="logout()">
		<img src = "images/logout.png" alt = "logout button">
	</a>
</div>

<div class="unsorted">
<%
final boolean sessionstatus=(session.getAttribute("Databasename")==null);

if(sessionstatus)
	response.sendRedirect("Homepage.html");
	
else{
		String columnnameslist[]=(String[]) session.getAttribute("columnnameslist");
		String Databasename=(String) session.getAttribute("Databasename");
		String Tablename=(String) session.getAttribute("Tablename");
		out.println("<p>BEFORE SORT</p><div class='sortedtable'>");
		out.println("<table bgcolor='LavenderBlush' width='200px'><tr>");
		
		for(String columname:columnnameslist ){		
		out.print("<th>"+columname.toUpperCase()+"</th>");
}
out.println("</tr>");
ResultSet unsortedresultset=DBconnector.unarrangeddata(Databasename,Tablename);
while(unsortedresultset.next())
{
	out.println("<tr>");
	for(String columname:columnnameslist ){
	out.println("<td>"+unsortedresultset.getObject(columname)+"</td>");
	}
	out.println("</tr>");

} 
out.println("</table></div><br>");
}
%>


</div>
<script>
function logout() {
    var result = confirm("Are you sure you want to logout?");
    if (result) {
        window.location.replace("sessioninvalidate.jsp");
    }
}
</script>

</body>
</html>