<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.util.*,java.sql.*" %>
<%@ page import="java.io.PrintWriter" %>
<%@page import="Model.DBconnector"%>
<%@page import="controller.Tablename"%>
 <%
    response.addHeader("Cache-Control","no-cache,no-store,must-revalidate"); 
   response.addHeader("Pragma","no-cache");
    response.addDateHeader ("Expires", 0);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="Style.css">
</head>

<body bgcolor="LavenderBlush" >

<div class="Back">

	<a href ="sessioninvalidate.jsp">
		<img src = "images/back.png" alt = "back button">
	</a>
	
</div>
<div class="logout">

<a href="#" onclick="logout()">
		<img src = "images/logout.png" alt = "logout button">
	</a>
	
</div>


<div class="fetchtable" align="center">
<form action="Tablename" method="post">

<br><br>
<label>Select the Table name to sort the column in ascending order<br></label>
<select name="Tablename">
  <%
  if(session.getAttribute("Databasename")==null){
		response.sendRedirect("Homepage.html");
	}
  else{
  String Databasename=(String)session.getAttribute("Databasename");
  ResultSet resultsettable=DBconnector.tables(Databasename);
	while(resultsettable.next())
	{
		String Tablename=resultsettable.getString("TABLE_NAME");
		out.println("<option value="+Tablename+" style=font-size:20px>"+Tablename+"</option>");
	}
  }
%>
</select>
  <br><br>
  <input type="hidden" id="hiddenField"/>
<input type="submit" value="submit">

</form>
</div>
<script>
function logout() {
    var result = confirm("Are you sure you want to logout?");
    if (result) {
        window.location.replace("sessioninvalidate.jsp");
    	}
}
</script>

</body>

</html>