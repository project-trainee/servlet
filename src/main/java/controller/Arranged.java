package controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.DBconnector;

@WebServlet("/Arranged")
public class Arranged extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session= request.getSession();
		String columnnameslist[]=(String[]) session.getAttribute("columnnameslist");
		String Databasename=(String) session.getAttribute("Databasename");
		String Tablename=(String) session.getAttribute("Tablename");
		session.setAttribute("columnnameslist", columnnameslist);
		ArrayList<ArrayList<?>> allcolumnsortvalues =new ArrayList<>();

		try {

		        for(String columnname: columnnameslist) {
		    		ArrayList<Object> eachcolumnvalues = new ArrayList<>();
		        ResultSet unsortedresultset=DBconnector.unarrangeddata(Databasename, Tablename);
					while(unsortedresultset.next()){
						eachcolumnvalues.add(unsortedresultset.getObject(columnname));
					} 
	
				ArrayList<?> eachcolumnsortedvalues=Columndatasort.sortedcolumn(eachcolumnvalues);
				allcolumnsortvalues=Columndatasort.addsortedArraylist(allcolumnsortvalues,eachcolumnsortedvalues);
		        }
	        
			session.setAttribute("sortedarray", allcolumnsortvalues);
		
			response.sendRedirect("Display.jsp");
		
	    }
		catch (SQLException | ClassNotFoundException e) {
		    e.printStackTrace();
		}
	}}