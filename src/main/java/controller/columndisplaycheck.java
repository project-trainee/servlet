package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/columndisplaycheck")
public class columndisplaycheck extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		final String columnnameslist[]=request.getParameterValues("columnnameslist");
		HttpSession session= request.getSession();
		session.setAttribute("columnnameslist", columnnameslist);
		final String view=request.getParameter("view");
		final boolean columnamescheck=(columnnameslist==null);
		if(columnamescheck) {
			PrintWriter out = response.getWriter();
			String alert = "SELECT ATLEAST ANYONE COLUMN";
		    out.println("<script>");
		    out.println("alert('" + alert + "');");
		    out.println("window.location.replace('Fetchcolumn.jsp');");
		    out.println("</script>");
		}
		else {
			final boolean radiobutton=view.equals("viewonly");
			if(radiobutton) {
				response.sendRedirect("viewtable.jsp");
			}
			else {

				response.sendRedirect("Arranged");
			}
		}

	}

}
