package controller;

import java.util.ArrayList;
import java.util.Collections;

public class Columndatasort {

	
	public static <T> ArrayList<ArrayList<?>> addsortedArraylist(ArrayList<ArrayList<?>> allcolumnsortvalues,ArrayList<T> values)
		{
			allcolumnsortvalues.add(values);
			return allcolumnsortvalues;
		}
	@SuppressWarnings("unchecked")
	public static ArrayList<?> sortedcolumn(ArrayList<?> sortedarray ) {


            if (sortedarray.get(0) instanceof Integer) {
	                Collections.sort((ArrayList<Integer>) sortedarray, (o1, o2) -> {
	                   if (o1 > o2)
	                       return 1;
	                   else if (o2 > o1)
	                       return -1;
	                   else
	                       return 0;
	               });

	                }
            else if (sortedarray.get(0) instanceof Long) {
                Collections.sort((ArrayList<Long>) sortedarray, (o1, o2) -> {
                   if (o1 > o2)
                       return 1;
                   else if (o2 > o1)
                       return -1;
                   else
                       return 0;
               });

                }
	       else if (sortedarray.get(0) instanceof Float) {
	           Collections.sort((ArrayList<Float>) sortedarray, (o1, o2) -> {
	               if (o1 > o2)
	                   return 1;
	               else if (o2 > o1)
	                   return -1;
	               else
	                   return 0;
	           });
	       		}
	      else if (sortedarray.get(0) instanceof String) {

	           Collections.sort((ArrayList<String>) sortedarray, (o1, o2) -> {
	               if (o1.length() > o2.length())
	                   return 1;
	               else if (o2.length() > o1.length())
	                   return -1;
	               else
	                   return 0;
	           }
	           );}
	      else {
	           Collections.sort((ArrayList<Double>) sortedarray, (o1, o2) -> {
	               if (o1 > o2)
	                   return 1;
	               else if (o2 > o1)
	                   return -1;
	               else
	                   return 0;
	           });
	    }
		return sortedarray; 
	}
}
