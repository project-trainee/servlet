package Model;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;

@WebServlet("/DBconnector")
public class DBconnector{
	public static Connection connect_DB(String Databasename) throws ClassNotFoundException, SQLException {

        try {
            Class.forName("org.postgresql.Driver");
            Connection connect = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + Databasename, "postgres", "Senthil@123");
            return connect;
        }
        catch(Exception e) {
            return (Connection) e;
        }

    }
	
public static ResultSet catalog(String Databasename) throws SQLException, ClassNotFoundException {
		
		Connection connect = DBconnector.connect_DB(Databasename);
		DatabaseMetaData meta = connect.getMetaData();
		ResultSet catalogresultset=meta.getCatalogs();
		connect.close();
		return catalogresultset;
		
	}

public static ResultSet tables(String Databasename) throws SQLException, ClassNotFoundException {
	
	Connection connect = DBconnector.connect_DB(Databasename);
	DatabaseMetaData meta = connect.getMetaData();
	ResultSet tablesresulset= meta.getTables(null, null, null, new String[] {"TABLE"});
	connect.close();
	return tablesresulset;
	
}
public static ResultSet columns(String Tablename,String Databasename) throws SQLException, ClassNotFoundException {
	
	Connection connect = DBconnector.connect_DB(Databasename);
	DatabaseMetaData meta = connect.getMetaData();
	ResultSet columnsresulset= meta.getColumns(null,null,Tablename,null);
	connect.close();
	return columnsresulset;
	
}
	
public static ResultSet unarrangeddata(String Databasename,String Tablename) throws SQLException, ClassNotFoundException {
	Connection connect = DBconnector.connect_DB(Databasename);
	String query="SELECT * FROM "+Tablename;
	PreparedStatement stm = connect.prepareStatement(query);
	ResultSet data= stm.executeQuery();
	connect.close();
	return data;
	
}

	

}
